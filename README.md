# Checklist Principes Common Ground

### Inleiding

De eerste teams in de Common Ground community leveren de eerste producten op. Wat kunnen we van hen leren? Welke 'best practices' zijn er? En kunnen we die vertalen naar een checklist om met nieuwe ontwikkelteams concrete afspraken te maken over de manier waarop bestaande of nieuwe producten/componenten gaan voldoen aan de principes van Common Ground?

Naar aanleiding van de gesprekken tussen de ontwikkelteams op PI events en de [concretisering van de meerjarige transitiestrategie](https://vng.nl/sites/default/files/2020-06/20200617_common-ground-transitiestrategie.pdf), ontwikkelen we een Checklist Common Ground voor het maken van software volgens de principes van Common Ground.

### Doel van de checklist

Het doel van de checklist is om relatief snel te kunnen inschatten of producten/componenten compatibel zijn met de informatiekundige visie Common Ground. De checklist is een hulpmiddel waar puntsgewijs een aantal vragen/statements worden afgelopen.  

Op het Fieldlab Digitale Overheid in Transitie heeft er een werksessie/meetUp plaatsgevonden waar een voorzet is besproken van zo’n een mogelijke CG checklist. Tijdens de werksessie is de werkwijze en vorm van de checklist besproken, en hebben we gesproken over de inhoud van de checklist. Afgesproken is om het gesprek open en transparant voort te zetten op Gitlab.

Graag betrekken we developers uit de huidige ontwikkelteams – die de informatiekundige visie van CG in software implementeren – om de best practices te verzamelen om de voorzet van de checklist te verfijnen tot een hulpmiddel wat in de praktijk bruikbaar is. 

### Hoe werkt de checklist?

De informatiekundige visie van Common Ground bestaat uit 13 principes verdeeld over twee documenten: 

- Het document [Realisatieprincipes](https://appstore.commonground.nl/20190130_-_Common_Ground_-_Realisatieprincipes.pdf) bevat zeven principes die aangeven welke werkwijze gehanteerd wordt bij het maken van Common Ground-software. 
- Het document [Informatiearchitectuurprincipes]() bevat zes principes die aangeven hoe de informatiearchitectuur van Common Ground-software in elkaar zit.

Deze 13 principes zijn helder maar ook abstract. In de checklist vertalen we de principes naar de praktijk en maken we deze meetbaar. Om zaken meetbaar te maken moet de grootheid en de eenheid bepaald worden: wat wordt gemeten, en in welke eenheden meet je dat?

### Waar staat de checklist?

De meest actuele versie van de checklist is [hier](https://gitlab.com/commonground/docs/checklist/-/blob/master/checklist.yaml) in dit Gitlab-repository te vinden.

Per principe zijn er twee lijstjes opgenomen:

- practice: een lijstje met praktische toepassingen van het principe. Ieder item in de lijst is als het ware een antwoord op de vraag: "wat willen we meten?".
- smart: een lijstje met (zo smart mogelijk) een concrete manier waarop dit principe gemeten kan worden. Ieder item in de lijst is een antwoord op de vraag "Op welke manier kunnen we dit principe meten?".

Een nuttige analogie is die met het meten van een boom:

- Een aspect dat je van een boom zou kunnen meten is de hoogte/lengte (dat is dan de grootheid die je meet).
- Een manier om hoogte/lengte te meten, is het meten van het aantal centimeters (dat is dan de eenheid die je meet).

De checklist gebruikt het [YAML-formaat](https://yaml.org/) opdat deze zowel door mensen als software gemakkelijk gelezen kan worden. Dit laatste met het oog op het automatiseren van een aantal processen in het maken, beheren en gebruiken van de checklist.

### Op welke manier kun je bijdragen?

1) Pak de documenten van de [principes](https://appstore.commonground.nl/over) en lees deze goed door. Als je een developer bent uit een ontwikkelteam hou dan de volgende vragen in je achterhoofd: waar ben ik tegenaan gelopen? Welke keus heb ik gemaakt en wat werkt het best? Kies bij voorkeur één van de principes om in meer detail naar te kijken.

2) Beantwoord het volgende: 

    Hoe kan dit principe in de praktijk worden toegepast? 

    Hoe kan dit principe meetbaar worden gemaakt? Maak het zo SMART mogelijk. 

3) Doe een voorstel voor het toevoegen of aanpassen van een praktische toepassing en/of meetmethode in Gitlab. Dit kan door een MR te maken met de voorgestelde wijzigingen. Voeg waar nodig onderbouwing of voorbeelden/uitleg toe in de MR. Wees zo concreet mogelijk, daag jezelf uit en nodig anderen uit om ook deel te nemen, of vraag bijvoorbeeld om feedback van de community in de Common Ground slack. 

**:crystal_ball: Timeline**

- Augustus - september: Vragenlijst staat online op GitLab, kennis verzamelen, opstellen eerste conceptversie.
- 6 Oktober: Meetup tijdens Fieldlab, bespreken eerste conceptversie.
- Oktober: online discussie, opstellen eerste documentversie.
- November: eerste documentversie opleveren.